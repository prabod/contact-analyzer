<!DOCTYPE html>
<html>
<head>

<script src='css/jquery.min.js'></script>

<link rel="stylesheet" href="css/style.css">
        
        <title>Contact Analyzer</title>

        <header>
            <h1>Main Report</h1>

        </header>

    <style type="text/css">


        .error{
            color: #FF0000;

        }
        .time{
            font-size: 10px;
        }
        table {
            color: #333;
            font-family: Helvetica, Arial, sans-serif;
            width: auto;
            border-collapse:collapse; 
            border-spacing: 0;
            alignment-adjust:central;
            width: 100%;
        }

        th, td {
            border: 1px solid transparent; /* No more visible border */
            height: 10px;
            transition: all 0.3s;  /* Simple transition for hover effect */
        }

        th {
            background: #ed1c24;
            color: #FEFEFE;
            font-size: 13px;
            min-width: 50px;
        }

        td {
            background: #FAFAFA;
            text-align: center;
            font-size: 12px;

        }


        /* Cells in even rows (2,4,6...) are one color */
        tr:nth-child(even) td { background: #F1F1F1; }

        /* Cells in odd rows (1,3,5...) are another (excludes header cells)  */
        tr:nth-child(odd) td { background: #FEFEFE; }

        tr td:hover { background: #666; color: #FFF; } /* Hover cell effect! */
    </style>
    <script src='jquery.min.js'></script>
</head>
<body>
<?php
session_start();

function validateDate($date, $format = 'Y-m-d H:i')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

$_SESSION["cli"] = $_POST['cli'];
$_SESSION["skills"] = $_POST['skills'];
$_SESSION["from"] = $_POST['from'];
$_SESSION["to"] = $_POST['to'];
//$_SESSION["limit"] = $_POST['limit'];


$cli = $_SESSION["cli"];
//echo $cli."<br>";
$skills = $_SESSION["skills"];
//echo $skills."<br>";
//$skills = 320;
$from=$_SESSION["from"];
//echo $from."<br>";
$to=$_SESSION["to"];
//echo $to."<br>";

function genSkey($date){
    $now = strtotime($date); // or your date as well
     $your_date = strtotime("2016-06-29");
     $datediff = $now - $your_date;
     $kIoDValue = floor($datediff/(60*60*24));
     //echo $kIoDValue;
     $kvalue = 24287 + $kIoDValue;
     return $kvalue;
}
$fromKey = genSkey($from);
//echo $fromKey;
$toKey = genSkey($to);
//echo $toKey;



// Reporting with CLI and skills..........................

if ((isset($from)) && (isset($to)) && (!empty($skills)) && (!empty($cli)))
{
//$_SESSION["cli"] = $_POST['cli'];
//$_SESSION["skills"] = $_POST['skills'];
//$_SESSION["from"] = $_POST['from'];
//$_SESSION["to"] = $_POST['to'];
//$_SESSION["limit"] = $_POST['limit'];



//$cli = $_SESSION["cli"];
//$skills = $_SESSION["skills"];
//$skills = 320;
//$from=$_SESSION["from"];
//$to=$_SESSION["to"];
//$_SESSION["status"]=null;
//$limit = $_SESSION["limit"];



//var_dump(validateDate('$from'));

//echo $skills;

//echo $_POST['to'].$_POST['from'];
// Instructions if $_POST['value'] exist
//$conn = oci_connect('REPOSITORY', 'repository', 'AICNEWRACK');

$host        = "host=192.168.77.171";
$port        = "port=5432";
$dbname      = "dbname=ahadatamart";
$credentials = "user=postgres password=postgres";

$db = pg_connect( "$host $port $dbname $credentials"  );
if(!$db){
    echo "Error : Unable to open database\n";
    exit;
}

// generate and execute a query...............

$sql =<<<EOF
                select * from (select *,s1.skillnumber  as "skill",j1.agentreleasedvalue,  a1.agentname from skilldim s1, callsegmentfact c1,agentdim a1,junkdim j1  where exists (select * from callsegmentfact c2 where callingparty='$cli' and exists (select * from callsegmentfact c3 where skill1key in($skills) and c2.c2gid=c3.c2gid) and segmentstopdatekey>='$fromKey' and segmentstopdatekey<='$toKey' and c1.c2gid=c2.c2gid  and callsegment=1 ) and s1.skillkey=c1.skill1key and a1.agentkey=c1.answeringagentkey   and c1.junkkey=j1.junkkey order by c2gid,callsegment) c3 where segmentstoptime between '$from' and '$to';
EOF;

$ret = pg_query($db, $sql) or die("Error in query: $sql." . pg_last_error($db));

if(!$ret){
    echo pg_last_error($db);
    exit;
}

$arr = pg_fetch_all($ret);

        //print_r($arr);

if ($arr === false) {
            echo "data not found...!!!";
}
else{

?>
<form action="exporttoexcel.php" method="post" 
onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>

<table width="600px" cellpadding="2" cellspacing="2" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><input type="hidden" id="datatodisplay" name="datatodisplay">
        <input type="submit" value="Export to Excel">
      </td>
    </tr>
  </table>

  <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="myClass">

        <?php

        //set data to tha table...............

             echo "<tr>
                <th>Contact ID</th><th>Call seg fact</th><th>Call ID</th><th>Call segment</th><th>start time</th><th>stop time</th><th>calling party</th><th>Dialled Number</th><th>Disposition Time</th><th>Queue Time</th><th>Duration</th><th>Orig. Agent Login ID</th><th>Skill key</th><th>Answering Agent</th><th>talktime</th><th>Agent After Call Time</th><th>Hold Time</th><th>Time Held</th><th>Agent Released</th>
                </tr>";

        foreach($arr as $innerrow){

            echo "<tr><td>".$innerrow["c2gid"]."</td><td>".$innerrow["callsegmentfactkey"]."</td><td>".$innerrow["callid"]."</td><td>".$innerrow["callsegment"]."</td><td>".$innerrow["segmentstarttime"]."</td><td>".$innerrow["segmentstoptime"]."</td><td>".$innerrow["callingparty"]."</td><td>".$innerrow["dialednumber"]."</td><td>".$innerrow["consulttime"]."</td><td>".$innerrow["queuetime"]."</td><td>".$innerrow["duration"]."</td><td>".$innerrow["originalagentkey"]."</td><td>".$innerrow["skillnumber"]."</td><td>".$innerrow["agentlogid"]."</td><td>".$innerrow["talktime"]."</td><td>".$innerrow["collecteddigits"]."</td><td>".$innerrow["holdtime"]."</td><td>".$innerrow["timeheld"]."</td>";

            if ($innerrow["agentreleasedvalue"] ==0) {
                echo "<td> true</td>";
            }else{
                echo "<td> false </td>";
            }
            //<td>".$innerrow["agentreleasedvalue"]."</td>
            "</tr>";

        }
    }

        ?>
        <?php
}

// Reporting with CLI..............

elseif ((isset($from)) && (isset($to)) && (!empty($cli)) && (empty($skills))) {

//$_SESSION["cli"] = $_POST['cli'];
//$_SESSION["skills"] = $_POST['skills'];
//$_SESSION["from"] = $_POST['from'];
//$_SESSION["to"] = $_POST['to'];
//$_SESSION["limit"] = $_POST['limit'];


//$cli = $_SESSION["cli"];
//$skills = $_SESSION["skills"];
//$skills = 320;
//$from=$_SESSION["from"];
//$to=$_SESSION["to"];
//$limit = $_SESSION["limit"];

//echo $skills;

//var_dump(validateDate('$from'));

//echo $_POST['to'].$_POST['from'];
// Instructions if $_POST['value'] exist
//$conn = oci_connect('REPOSITORY', 'repository', 'AICNEWRACK');

$host        = "host=192.168.77.171";
$port        = "port=5432";
$dbname      = "dbname=ahadatamart";
$credentials = "user=postgres password=postgres";

$db = pg_connect( "$host $port $dbname $credentials"  );
if(!$db){
    echo "Error : Unable to open database\n";
    exit;
}

// generate and execute a query...............

$sql =<<<EOF
            select * from (select *,s1.skillnumber  as "skill",j1.agentreleasedvalue,  a1.agentname from skilldim s1, callsegmentfact c1,agentdim a1,junkdim j1  where exists (select * from callsegmentfact c2 where callingparty='$cli' and segmentstopdatekey>='$fromKey' and segmentstopdatekey<='$toKey'  and c1.c2gid=c2.c2gid  and callsegment=1 ) and s1.skillkey=c1.skill1key and a1.agentkey=c1.answeringagentkey   and c1.junkkey=j1.junkkey order by c2gid,callsegment) c3 where segmentstoptime between '$from' and '$to';
EOF;

$ret = pg_query($db, $sql) or die("Error in query: $sql." . pg_last_error($db));

if(!$ret){
    echo pg_last_error($db);
    exit;
}


$arr = pg_fetch_all($ret);



if ($arr === false) {
            echo "data not found...!!!";
}
else{

?>
<form action="exporttoexcel.php" method="post" 
onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>

<table width="600px" cellpadding="2" cellspacing="2" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><input type="hidden" id="datatodisplay" name="datatodisplay">
        <input type="submit" value="Export to Excel">
      </td>
    </tr>
  </table>

  <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="myClass">



        <?php


        //set data to tha table...............

            echo "<tr>
                <th>Contact ID</th><th>Call seg fact</th><th>Call ID</th><th>Call segment</th><th>start time</th><th>stop time</th><th>calling party</th><th>Dialled Number</th><th>Disposition Time</th><th>Queue Time</th><th>Duration</th><th>Orig. Agent Login ID</th><th>Skill key</th><th>Answering Agent</th><th>talktime</th><th>Agent After Call Time</th><th>Hold Time</th><th>Time Held</th><th>Agent Released</th>
                </tr>";

        foreach($arr as $innerrow){

            echo "<tr><td>".$innerrow["c2gid"]."</td><td>".$innerrow["callsegmentfactkey"]."</td><td>".$innerrow["callid"]."</td><td>".$innerrow["callsegment"]."</td><td>".$innerrow["segmentstarttime"]."</td><td>".$innerrow["segmentstoptime"]."</td><td>".$innerrow["callingparty"]."</td><td>".$innerrow["dialednumber"]."</td><td>".$innerrow["consulttime"]."</td><td>".$innerrow["queuetime"]."</td><td>".$innerrow["duration"]."</td><td>".$innerrow["originalagentkey"]."</td><td>".$innerrow["skillnumber"]."</td><td>".$innerrow["agentlogid"]."</td><td>".$innerrow["talktime"]."</td><td>".$innerrow["collecteddigits"]."</td><td>".$innerrow["holdtime"]."</td><td>".$innerrow["timeheld"]."</td>";

             if ($innerrow["agentreleasedvalue"] ==0) {
                echo "<td> true</td>";
            }else{
                echo "<td> false </td>";
            }
            //<td>".$innerrow["agentreleasedvalue"]."</td>
            "</tr>";

        }
    }

        ?>
        <?php

// Reporting without skills and CLI................

}elseif ((isset($from)) && (isset($to)) && (empty($cli)) && (empty($skills))) {

    //$_SESSION["cli"] = $_POST['cli'];
//$_SESSION["skills"] = $_POST['skills'];
//$_SESSION["from"] = $_POST['from'];
//$_SESSION["to"] = $_POST['to'];
//$_SESSION["limit"] = $_POST['limit'];


//$cli = $_SESSION["cli"];
//$skills = $_SESSION["skills"];
//$skills = 320;
//$from=$_SESSION["from"];
//$to=$_SESSION["to"];
//$limit = $_SESSION["limit"];

//var_dump(validateDate($from) && validateDate($to));

//echo $skills;
//echo $from."<br>";
//echo $to;
//echo $_POST['to'].$_POST['from'];
// Instructions if $_POST['value'] exist
//$conn = oci_connect('REPOSITORY', 'repository', 'AICNEWRACK');

$host        = "host=192.168.77.171";
$port        = "port=5432";
$dbname      = "dbname=ahadatamart";
$credentials = "user=postgres password=postgres";

$db = pg_connect("$host $port $dbname $credentials");
if(!$db){
    echo "Error : Unable to open database\n";
    exit;
}

// generate and execute a query...............

$sql =<<<EOF
        select * from (select *,s1.skillnumber  as "skill",j1.agentreleasedvalue,  a1.agentname from skilldim s1, callsegmentfact c1,agentdim a1,junkdim j1  where exists (select * from callsegmentfact c2 where segmentstopdatekey>='$fromKey' and segmentstopdatekey<='$toKey' and c1.c2gid=c2.c2gid ) and s1.skillkey=c1.skill1key and a1.agentkey=c1.answeringagentkey  and c1.junkkey=j1.junkkey order by c2gid,callsegment) c3 where segmentstoptime between '$from' and '$to' limit 1765958 offset 0;
EOF;

$ret = pg_query($db, $sql) or die("Error in query: $sql." . pg_last_error($db));

if(!$ret){
    echo pg_last_error($db);
    exit;
}


$arr = pg_fetch_all($ret);
//echo "bhjghjgvhdgjhds";
        //print_r($arr);

if ($arr === false) {
            //echo "data not found...!!!";
}
else{

?>



<form action="exporttoexcel.php" method="post" 
onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>

<table width="600px" cellpadding="2" cellspacing="2" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><input type="hidden" id="datatodisplay" name="datatodisplay">
        <input type="submit" value="Export to Excel">
      </td>
    </tr>
  </table>

  <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="myClass">


        <?php

        //set data to tha table...............

             echo "<tr>
                <th>Contact ID</th><th>Call seg fact</th><th>Call ID</th><th>Call segment</th><th>start time</th><th>stop time</th><th>calling party</th><th>Dialled Number</th><th>Disposition Time</th><th>Queue Time</th><th>Duration</th><th>Orig. Agent Login ID</th><th>Skill key</th><th>Answering Agent</th><th>talktime</th><th>Agent After Call Time</th><th>Hold Time</th><th>Time Held</th><th>Agent Released</th>
                </tr>";

        foreach($arr as $innerrow){

            echo "<tr><td>".$innerrow["c2gid"]."</td><td>".$innerrow["callsegmentfactkey"]."</td><td>".$innerrow["callid"]."</td><td>".$innerrow["callsegment"]."</td><td>".$innerrow["segmentstarttime"]."</td><td>".$innerrow["segmentstoptime"]."</td><td>".$innerrow["callingparty"]."</td><td>".$innerrow["dialednumber"]."</td><td>".$innerrow["consulttime"]."</td><td>".$innerrow["queuetime"]."</td><td>".$innerrow["duration"]."</td><td>".$innerrow["originalagentkey"]."</td><td>".$innerrow["skillnumber"]."</td><td>".$innerrow["agentlogid"]."</td><td>".$innerrow["talktime"]."</td><td>".$innerrow["collecteddigits"]."</td><td>".$innerrow["holdtime"]."</td><td>".$innerrow["timeheld"]."</td>";

            //$status = $innerrow["agentreleasedvalue"];
            //echo $status;

            if ($innerrow["agentreleasedvalue"] ==0) {
                echo "<td> true</td>";
            }else{
                echo "<td> false </td>";
            }
            //<td>".$innerrow["agentreleasedvalue"]."</td>
            "</tr>";

        }
    }

        ?>
        <?php
}
else{
    echo "Wrong inputs. Recheck again.........";
}
        ?>
    </table>

</form>


</body>
</html>