<!DOCTYPE html>
<html>
<head>

<script src='css/jquery.min.js'></script>

<link rel="stylesheet" href="css/style.css">
        
        <title>Contact Analyzer</title>

        <header>
            <h1>Repeat Callers Report</h1>

        </header>

    <style type="text/css">


        .error{
            color: #FF0000;

        }
        .time{
            font-size: 10px;
        }
        table {
            color: #333;
            font-family: Helvetica, Arial, sans-serif;
            width: auto;
            border-collapse:collapse; 
            border-spacing: 0;
            alignment-adjust:central;
            width: 100%;
        }

        th, td {
            border: 1px solid transparent; /* No more visible border */
            height: 10px;
            transition: all 0.3s;  /* Simple transition for hover effect */
        }

        th {
            background: #ed1c24;
            color: #FEFEFE;
            font-size: 13px;
            min-width: 50px;
        }

        td {
            background: #FAFAFA;
            text-align: center;
            font-size: 12px;

        }


        /* Cells in even rows (2,4,6...) are one color */
        tr:nth-child(even) td { background: #F1F1F1; }

        /* Cells in odd rows (1,3,5...) are another (excludes header cells)  */
        tr:nth-child(odd) td { background: #FEFEFE; }

        tr td:hover { background: #666; color: #FFF; } /* Hover cell effect! */
    </style>
    <script src='jquery.min.js'></script>
 </head>
 <body>
    <?php
 	session_start();

  $_SESSION["from"] = $_POST['from'];
  $_SESSION["to"] = $_POST['to'];
  $_SESSION["limit"] = $_POST['limit'];

  $skills = $_SESSION["skills"];
  $from=$_SESSION["from"];
  $to=$_SESSION["to"];

//Reporting without skills.......

 	if ((isset($from)) &&  (isset($to)) && (empty($skills))) 
	{    
            //$_SESSION["skills"] = $_POST['skills'];
			//$_SESSION["from"] = $_POST['from'];
			//$_SESSION["to"] = $_POST['to'];
            //$_SESSION["limit"] = $_POST['limit'];

            //$skills = $_SESSION["skills"];
			//$from=$_SESSION["from"];
			//$to=$_SESSION["to"];
            //$limit = $_SESSION["limit"];

//echo "Without skills.....!!!";
            //echo $from;
            //echo $to;
			//echo $_POST['to'].$_POST['from'];
          // Instructions if $_POST['value'] exist    
		   //$conn = oci_connect('REPOSITORY', 'repository', 'AICNEWRACK');

            $host        = "host=192.168.77.171";
            $port        = "port=5432";
            $dbname      = "dbname=ahadatamart";
            $credentials = "user=postgres password=postgres";

            $db = pg_connect( "$host $port $dbname $credentials"  );
            if(!$db){
                echo "Error : Unable to open database\n";
                exit;
            }

 // generate and execute a query...............

            $sql =<<<EOF
                select n1.date,skilldim.skillname as skill,sum(n1.count) as total_calls,sum(case when n1.count=1 then 1 else 0 end) as unq_calls,sum(case when n1.count=1 then 0 else 1 end) as rpt_calls from(select to_char(segmentstarttime,'mm/dd/yy') as date,skill1key,callingparty,count(callingparty) as count from callsegmentfact where skill1key!=1  and segmentstarttime between '$from' and '$to' group by date,skill1key,callingparty) n1,skilldim where n1.skill1key=skilldim.skillkey group by n1.date,n1.skill1key,skilldim.skillname order by n1.date,n1.skill1key,skilldim.skillname;
EOF;

   $ret = pg_query($db, $sql) or die("Error in query: $sql." . pg_last_error($db));
   
   if(!$ret){
      echo pg_last_error($db);
      exit;
   }


    $arr = pg_fetch_all($ret);

    if ($arr === false) {
            echo "data not found...!!!";
}
else{

    ?>
	<form action="exporttoexcel.php" method="post" 
onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>

<table width="600px" cellpadding="2" cellspacing="2" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><input type="hidden" id="datatodisplay" name="datatodisplay">
        <input type="submit" value="Export to Excel">
      </td>
    </tr>
  </table>

  <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="myClass">
      <?php

    //set data to tha table...............

	echo "<tr>
                <th>Date</th>
                <th>Skill</th>
                <th>Total CAlls</th>
                <th>Unique Calls</th>
                <th>Repeat Calls</th>
                
                
			   </tr>";

        

        //print_r($arr);
   
         foreach($arr as $innerrow){

                 echo "<tr><td>".$innerrow["date"]."</td><td>".$innerrow["skill"]."</td><td>".$innerrow["total_calls"]."</td><td>".$innerrow["unq_calls"]."</td><td>".$innerrow["rpt_calls"]."</td></tr>";
             }
         }
		   
        ?>
         <?php
 }



 //Reporting with skills..........

 elseif ((isset($from)) &&  (isset($to)) && (!empty($skills))) {

    //$_SESSION["skills"] = $_POST['skills'];
    //$_SESSION["from"] = $_POST['from'];
    //$_SESSION["to"] = $_POST['to'];
            //$_SESSION["limit"] = $_POST['limit'];

    //$skills = $_SESSION["skills"];
    //$from=$_SESSION["from"];
    //$to=$_SESSION["to"];
            //$limit = $_SESSION["limit"];

//echo "with skills.....!!!";
    //echo $from;
    //echo $to;
            //echo $_POST['to'].$_POST['from'];
          // Instructions if $_POST['value'] exist    
           //$conn = oci_connect('REPOSITORY', 'repository', 'AICNEWRACK');

            $host        = "host=192.168.77.171";
            $port        = "port=5432";
            $dbname      = "dbname=ahadatamart";
            $credentials = "user=postgres password=postgres";

            $db = pg_connect( "$host $port $dbname $credentials"  );
            if(!$db){
                echo "Error : Unable to open database\n";
                exit;
            }

 // generate and execute a query...............

            $sql =<<<EOF
                select n1.date,skilldim.skillname as skill,sum(n1.count) as total_calls,sum(case when n1.count=1 then 1 else 0 end) as unq_calls,sum(case when n1.count=1 then 0 else 1 end) as rpt_calls from(select to_char(segmentstarttime,'mm/dd/yy') as date,skill1key,callingparty,count(callingparty) as count from callsegmentfact where skill1key!=1  and skill1key in(select skillkey from skilldim where skillnumber in($skills)) and segmentstarttime between '$from' and '$to' group by date,skill1key,callingparty) n1,skilldim where n1.skill1key=skilldim.skillkey group by n1.date,n1.skill1key,skilldim.skillname order by n1.date,n1.skill1key,skilldim.skillname;
EOF;

   $ret = pg_query($db, $sql) or die("Error in query: $sql." . pg_last_error($db));
   
   if(!$ret){
      echo pg_last_error($db);
      exit;
   }

    $arr = pg_fetch_all($ret);

        //print_r($arr);

if ($arr === false) {
            echo "data not found...!!!";
}
else{

    ?>
    <form action="exporttoexcel.php" method="post" 
onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>

<table width="600px" cellpadding="2" cellspacing="2" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><input type="hidden" id="datatodisplay" name="datatodisplay">
        <input type="submit" value="Export to Excel">
      </td>
    </tr>
  </table>

  <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="myClass">
      <?php

    //set data to tha table...............

    echo "<tr>
                <th>Date</th>
                <th>Skill</th>
                <th>Total CAlls</th>
                <th>Unique Calls</th>
                <th>Repeat Calls</th>
                
                
               </tr>";
        
   
         foreach($arr as $innerrow){

                 echo "<tr><td>".$innerrow["date"]."</td><td>".$innerrow["skill"]."</td><td>".$innerrow["total_calls"]."</td><td>".$innerrow["unq_calls"]."</td><td>".$innerrow["rpt_calls"]."</td></tr>";
             }
           }
        ?>
         <?php
 }
 else{
    echo "Wrong inputs....!!!";
 }
 ?>
		</table>
        
    </form>

</body>
</html>